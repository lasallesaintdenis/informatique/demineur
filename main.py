#!/usr/bin/python3

from random import randint

def init():
    grille = [[0 for i in range(10)] for i in range(10)]
    l, c = randint(0,len(grille)-1),randint(0,len(grille[0])-1)
    cpt = 0
    while cpt < 3:
        grille[l][c] = 1
        ll, cc = randint(0,len(grille)-1),randint(0,len(grille[0])-1)
        while (ll,cc) == (l,c):
            ll, cc = randint(0,len(grille)-1),randint(0,len(grille[0])-1)
        else:
            cpt = cpt + 1
            l,c = ll,cc
    affichage = [[' ' for i in range(10)] for i in range(10)]
    return grille,affichage

def est_mine(l,c, grille):
    return bool(grille[l][c])

def entree_ligne_colonne():
    l = int(input('ligne'))
    c = int(input('colonne'))
    return l,c

def compte_mines_autour(l,c, grille, affichage):
    if l == 0:
        compteur = grille[l][c-1] + grille[l+1][c-1] + grille[l+1][c] + grille[l][c+1] + grille[l+1][c+1]
    elif l == len(grille) - 1:
        compteur = grille[l-1][c-1] + grille[l][c-1] + grille[l-1][c] + grille[l-1][c+1] + grille[l][c+1]
    elif c == 0:
        compteur = grille[l-1][c] + grille[l+1][c]  + grille[l-1][c+1] + grille[l][c+1] + grille[l+1][c+1]
    elif c == len(grille[0]) - 1:
        compteur = grille[l-1][c-1] + grille[l][c-1] + grille[l+1][c-1] + grille[l-1][c] + grille[l+1][c]
    else:
        compteur = grille[l-1][c-1] + grille[l][c-1] + grille[l+1][c-1] + grille[l-1][c] + grille[l+1][c] + grille[l-1][c+1] + grille[l][c+1] + grille[l+1][c+1]
    affichage[l][c] = compteur
    return affichage


def affiche(affichage):
    print('-'*(2*len(affichage[0])+1))
    for i in affichage:
        print('|',end='')
        for j in i:
            print(j,sep='',end='|')
        print('\n','-'*(2*len(i)),sep='-')

def main():
    grille, affichage = init()
    nb_mines = 3
    nb_cases = len(grille)*len(grille[0]) - nb_mines
    nb_coup = 0
    continuer = True
    l, c = entree_ligne_colonne()
    while continuer :
        if est_mine(l,c, grille):
            affiche(affichage)
            return 'Fin de la partie'
        else:
            compte_mines_autour(l, c, grille, affichage)
            affiche(affichage)
            nb_coup = nb_coup + 1
            if nb_coup >= nb_cases:
                continuer = False
            l, c = entree_ligne_colonne()


if __name__ == '__main__':
    main()